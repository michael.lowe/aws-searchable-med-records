# import boto3
# from io import BytesIO
# import json
# import logging

# from resources.trp import Document

# logger = logging.getLogger('textract-logger')
# logger.setLevel(logging.INFO)


# def get_api_response(file_bytes: BytesIO, cmd: str=None, args: list=None) -> dict:
#     file_bytes_stream: bytes = file_bytes.getvalue() if file_bytes is not None else None
#     if not file_bytes_stream:
#         return None
#     logger.info(f"connecting to textract Web Service")
#     textract_client: boto3.client = boto3.client('textract', verify=False)
#     if cmd is None:
#         response: dict = textract_client.detect_document_text(Document={
#             'Bytes': file_bytes_stream
#      })
    
#     elif cmd == 'analyze' and args is not None:
#         response: dict = textract_client.analyze_document(Document={
#             'Bytes': file_bytes_stream,
#         },
#         FeatureTypes = args)

#     logger.info(f"response entity: {response}")
#     return response

# def extract_text(api_response: dict) -> str:
#     document: Document = Document(api_response)
#     text: list = []
#     for page in document.pages:
#         page_level_text: str = page.text
#         logger.info(f"appending page level data: {page_level_text}")
#         text.append(page_level_text)
#     return ' '.join(text) if len(text) != 0 else None

# def extract_tabels(api_response) -> dict:
#     document: Document = Document(api_response)
#     for page in document.pages:
#         for table in page.tables:
#             for row in table.rows:
#                 for cell in row.cells:
#                     print(cell.text)
        
# with open("/Users/michaellowe/Downloads/phi_free_1_1.png", "rb") as f:
#     input: BytesIO= BytesIO(f.read())

# # response: dict = get_api_response(input, cmd='analyze', args=["TABLES"])
# # print(extract_tabels(response))

# import pytesseract
# from PIL import Image

# pytesseract.pytesseract.tesseract_cmd='/Users/michaellowe/Utilities/brew/bin/tesseract'

# # pdf = pytesseract.image_to_pdf_or_hocr(image=Image.open(input), extension='pdf')
# # with open("/tmp/stuff.pdf", "wb") as f:
# #     f.write(pdf)

# data = pytesseract.image_to_data(image=Image.open(input))

from celery import Celery
from resources import celeryconfig
from kombu.utils.url import safequote

app: Celery = Celery()
app.config_from_object(celeryconfig)
@app.task(name='test_task')
def testing(args:str):
    return {'hello': 'passed'}
