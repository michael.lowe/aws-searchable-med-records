from dotenv import load_dotenv
from os import getenv
import urllib

load_dotenv(dotenv_path='./.env')
aws_access_key = getenv("AWS_ACCESS_KEY")
aws_secret_access_key = getenv("AWS_SECRET_ACCESS_KEY")
broker_transport: str = 'sqs'
broker_url: str = getenv("CELERY_BROKER_URL")
result_backend: str = getenv("CELERY_BACKEND_URL")
task_serializer: str = 'json'
result_serializer: str = 'json'
database_table_names = {
    'task': getenv("CELERY_TASK_DB"),
    'group': getenv("CELERY_GROUP_DB")
}