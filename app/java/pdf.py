from io import BytesIO
import boto3
from jpype import *
import jpype.imports as jImports

from resources.trp import Document

startJVM(
    getDefaultJVMPath(),
    "-ea",
    "-Djava.class.path=/Users/michaellowe/Utilities/packages/java/pdfbox-app-2.0.19.jar",
    convertStrings=True
)

jImports.registerDomain("org")

from org.apache.pdfbox.pdmodel import PDDocument 
from org.apache.pdfbox.pdmodel import PDPage 
from org.apache.pdfbox.pdmodel import PDPageContentStream
from org.apache.pdfbox.pdmodel.common import PDRectangle
from org.apache.pdfbox.pdfparser import PDFParser
from org.apache.pdfbox.pdmodel import PDPageContentStream
from org.apache.pdfbox.pdmodel.PDPageContentStream import AppendMode
from org.apache.pdfbox.pdmodel.graphics.image import PDImageXObject
from org.apache.pdfbox.pdmodel.graphics.image import PDImageXObject
from org.apache.pdfbox.pdmodel.font import PDFont
from org.apache.pdfbox.pdmodel.font import PDType1Font
from org.apache.pdfbox.pdmodel.graphics.state import RenderingMode

import org.apache.pdfbox.tools as tools


class PDFDoc:
    def __init__(self) -> None:
        self.__api_response: dict = None
        self.__document_binary: bytes = None
        self.__width: float = None
        self.__height: float = None
        self.__ocr_result: float = None
        self.__overlay_binary: float = None
        self.__pddoc: PDDocument = None
    @property
    def document_binary(self) -> bytes:
        return self.__document_binary
    
    @property
    def document_width(self) -> float:
        return self.__width
    
    @property
    def document_height(self) -> float:
        return self.__height

    @property
    def overlay_binary(self) -> bytes:
        return self.__overlay_binary
    
    @overlay_binary.setter
    def set_overlay_bianry(self, binary) -> None:
        self.__overlay_binary = binary

    @property
    def api_response(self) -> dict:
        return self.__api_response
    
    @api_response.setter
    def set_api_response(self, api_response) -> None:
        self.__api_response = api_response



pddoc: PDDocument = PDDocument()
file: java.io.File = java.io.File("/Users/michaellowe/Downloads/phi_free_1_1.png")
input_stream: java.io.FileInputStream = java.io.FileInputStream(file)
buffered_image: java.awt.image.BufferedImage = javax.imageio.ImageIO.read(input_stream)
print(f"image height: {buffered_image.getHeight()}")
width: float = buffered_image.getWidth()
height: float = buffered_image.getHeight()
page: PDPage = PDPage(PDRectangle(width, height))
pddoc.addPage(page)
img: PDImageXObject = PDImageXObject.createFromFileByContent(file, pddoc)
contentStream: PDPageContentStream = PDPageContentStream(pddoc, page)
contentStream.drawImage(img, 0, 0)
contentStream.close()
input_stream.close()
pddoc.save("/tmp/test.pdf")
pddoc.close()




b: BytesIO = BytesIO()

with open("/Users/michaellowe/Downloads/phi_free_1_1.png", "rb") as f:
    b.write(f.read())

textract: boto3.client = boto3.client('textract', verify=False)

response: dict = textract.detect_document_text(
    Document = {
        'Bytes': b.getvalue()
})


doc: Document = Document(response)
pages = doc.pages

new_pdf: PDDocument = PDDocument()
new_page: PDPage = PDPage()
font: PDFont = PDType1Font.TIMES_ROMAN
box: PDRectangle =  PDRectangle(width, height)
new_page.setMediaBox(box)
contentStream: PDPageContentStream =  PDPageContentStream(new_pdf, new_page, PDPageContentStream.AppendMode.APPEND,True,True)
contentStream.setRenderingMode(RenderingMode.NEITHER);
# font_size: int = 24
for line in pages[0].lines:
    font_size: int = line.geometry.boundingBox.height * height
    contentStream.setFont(font, font_size)
    contentStream.beginText()
    g_x: float = width*line.geometry.boundingBox.left
    g_y: float = height*(line.geometry.boundingBox.top+line.geometry.boundingBox.height)
    print("g_x: ", g_x)
    print("g_y: ", g_y)
    contentStream.moveTextPositionByAmount(g_x+5, height-g_y)
    contentStream.drawString(line.text)
    contentStream.endText()
contentStream.close()

new_pdf.addPage(new_page)
new_pdf.save("/tmp/test_2.pdf")
new_pdf.close()
args = []
args.extend(['/tmp/test_2.pdf'])
args.extend(['/tmp/test.pdf'])
args.extend(['/tmp/medrecord_result.pdf'])
tools.OverlayPDF.main(args)


shutdownJVM()

