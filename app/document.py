from io import BytesIO
import pytesseract

class DocData:
    # TODO: implement
    def __init__(self) -> None:
        self.__document: BytesIO = None
        self.__pages: BytesIO = None
        self.__ocr_result: list  = None
        self.__width = None
        self.__height = None
    
    @property
    def document(self) -> BytesIO:
        return self.__document

